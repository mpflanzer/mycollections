//
//  MyQueue.h
//  MyCollections
//
//  Created by Moritz Pflanzer on 04/03/2016.
//  Copyright © 2016 Moritz Pflanzer. All rights reserved.
//

#import "MyLinkedList.h"
#import <Foundation/Foundation.h>

@interface MyQueue : MyLinkedList

- (void)enqueueObject:(id)element;
- (id)dequeueObject;

@end
