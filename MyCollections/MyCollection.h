//
//  MyCollection.h
//  Collections
//
//  Created by Moritz Pflanzer on 26/02/2016.
//  Copyright © 2016 Moritz Pflanzer. All rights reserved.
//

#ifndef MyCollection_h
#define MyCollection_h

#import <Cocoa/Cocoa.h>

@protocol MyCollection

- (NSUInteger)count;

- (void)addObject:(id)object;
- (void)removeObject:(id)object;
- (BOOL)containsObject:(id)object;

@optional

- (void)removeAllObjects;

@end

#endif /* MyCollection_h */
