//
//  MyStack.h
//  MyCollections
//
//  Created by Moritz Pflanzer on 04/03/2016.
//  Copyright © 2016 Moritz Pflanzer. All rights reserved.
//

#import "MyCollection.h"
#import <Foundation/Foundation.h>

@interface MyStack : NSObject<MyCollection>

- (void)pushObject:(id)object;
- (id)popObject;
- (id)topObject;

@end
