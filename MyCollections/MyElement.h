//
//  MyElement.h
//  Collections
//
//  Created by Moritz Pflanzer on 26/02/2016.
//  Copyright © 2016 Moritz Pflanzer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyElement : NSObject

@property id object;
@property MyElement *prev;
@property MyElement *next;

- (instancetype)initWithObject:(id)object;

@end
