//
//  MyHashSet.m
//  Collections
//
//  Created by Moritz Pflanzer on 03/03/2016.
//  Copyright © 2016 Moritz Pflanzer. All rights reserved.
//

#import "MyHashSet.h"

@interface MyHashSet ()

@property NSMutableArray *buckets;

@end

@implementation MyHashSet

@end
