//
//  MyHashSet.h
//  Collections
//
//  Created by Moritz Pflanzer on 03/03/2016.
//  Copyright © 2016 Moritz Pflanzer. All rights reserved.
//

#import "MyCollection.h"
#import <Foundation/Foundation.h>

@interface MyHashSet : NSObject<MyCollection>

- (instancetype)initWithSize:(NSUInteger)size;

@end
