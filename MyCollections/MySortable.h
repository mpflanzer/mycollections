//
//  MySortable.h
//  MyCollections
//
//  Created by Moritz Pflanzer on 04/03/2016.
//  Copyright © 2016 Moritz Pflanzer. All rights reserved.
//

#ifndef MySortable_h
#define MySortable_h

@protocol MySortable

- (void)sort;

@optional

- (void)sortUsingBubbleSort;
- (void)sortUsingMergeSort;
- (void)sortUsingQuickSort;

@end


#endif /* MySortable_h */
