//
//  MyLinkedList.h
//  Collections
//
//  Created by Moritz Pflanzer on 26/02/2016.
//  Copyright © 2016 Moritz Pflanzer. All rights reserved.
//

#import "MyCollection.h"
#import "MySortable.h"
#import <Foundation/Foundation.h>

@class MyElement;

@interface MyLinkedList : NSObject<MyCollection, MySortable>

- (instancetype)initWithObjects:(NSArray*)elements;

- (void)appendObject:(id)object;
- (void)insertObject:(id)object atPosition:(NSUInteger)position;

- (void)removeLastObject;
- (void)removeObjectAtPosition:(NSUInteger)position;

- (id)firstObject;
- (id)lastObject;
- (id)objectAtPosition:(NSUInteger)position;

- (NSInteger)positionOfObject:(id)object;

- (void)reverse;

+ (instancetype)reverseLinkedList:(MyLinkedList*)list;

@end
