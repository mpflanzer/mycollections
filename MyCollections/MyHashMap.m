//
//  MyHashMap.m
//  Collections
//
//  Created by Moritz Pflanzer on 03/03/2016.
//  Copyright © 2016 Moritz Pflanzer. All rights reserved.
//

#import "MyHashMap.h"

@interface MyHashMap ()

@property NSMutableArray *buckets;
@property (readwrite) NSUInteger count;

@end

@implementation MyHashMap

@end
