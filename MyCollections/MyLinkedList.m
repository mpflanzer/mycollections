//
//  MyLinkedList.m
//  Collections
//
//  Created by Moritz Pflanzer on 26/02/2016.
//  Copyright © 2016 Moritz Pflanzer. All rights reserved.
//

#import "MyElement.h"
#import "MyLinkedList.h"

@interface MyLinkedList ()

@property MyElement *anchor;

@end

@implementation MyLinkedList

@end
