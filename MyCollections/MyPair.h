//
//  MyPair.h
//  Collections
//
//  Created by Moritz Pflanzer on 03/03/2016.
//  Copyright © 2016 Moritz Pflanzer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyPair : NSObject

@property id<NSCopying>key;
@property id object;

+ (instancetype)pairWithKey:(id<NSCopying>)key andObject:(id)object;

@end
