//
//  MyHashMap.h
//  Collections
//
//  Created by Moritz Pflanzer on 03/03/2016.
//  Copyright © 2016 Moritz Pflanzer. All rights reserved.
//

#import "MyCollection.h"
#import <Foundation/Foundation.h>

@interface MyHashMap : NSObject<MyCollection>

- (instancetype)initWithSize:(NSUInteger)size;

- (id)objectForKey:(id<NSCopying>)key;
- (void)setObject:(id)object forKey:(id<NSCopying>)key;
- (void)removeObjectForKey:(id<NSCopying>)key;

@end
