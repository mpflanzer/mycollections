//
//  main.m
//  MyCollections
//
//  Created by Moritz Pflanzer on 04/03/2016.
//  Copyright © 2016 Moritz Pflanzer. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSCAssert(0, @"Please use the tests to check the implementation!");
    }
    return 0;
}
