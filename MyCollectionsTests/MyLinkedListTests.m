//
//  MyLinkedListTests.m
//  MyCollectionsTests
//
//  Created by Moritz Pflanzer on 26/02/2016.
//  Copyright © 2016 Moritz Pflanzer. All rights reserved.
//

#import "MyLinkedList.h"
#import <XCTest/XCTest.h>

@interface MyLinkedListTests : XCTestCase

- (void)compareList:(MyLinkedList*)list toReversedList:(MyLinkedList*)reversedList;

@end

@implementation MyLinkedListTests

- (void)testDefaultCreation {
    MyLinkedList *emptyList = [MyLinkedList new];

    XCTAssertNotNil(emptyList, @"Creation of empty list failed!");
}

- (void)testCreationWithInitEmpty {
    MyLinkedList *anEmptyList = [[MyLinkedList alloc] initWithObjects:nil];

    XCTAssertNotNil(anEmptyList, @"Creation of empty list failed!");
    XCTAssertEqual([anEmptyList count], 0, @"List should be empty but has %lu elements!", [anEmptyList count]);

    MyLinkedList *anotherEmptyList = [[MyLinkedList alloc] initWithObjects:@[]];

    XCTAssertNotNil(anotherEmptyList, @"Creation of empty list failed!");
    XCTAssertEqual([anotherEmptyList count], 0, @"List should be empty but has %lu elements!", [anotherEmptyList count]);
}

- (void)testListCreationWithInitObjects {
    NSNumber *aNumber = @5;
    NSNumber *anotherNumber = @6;

    MyLinkedList *aList = [[MyLinkedList alloc] initWithObjects:@[aNumber, anotherNumber]];

    XCTAssertNotNil(aList, @"Creation of list failed!");
    XCTAssertEqual([aList count], 2, @"List should have 2 elements but has %lu elements!", [aList count]);
    XCTAssertEqual([aList objectAtPosition:0], aNumber, @"First object should be %@ but is %@!", aNumber, [aList objectAtPosition:0]);
    XCTAssertEqual([aList objectAtPosition:1], anotherNumber, @"Second object should be %@ but is %@!", aNumber, [aList objectAtPosition:1]);
}

- (void)testAppendObjects {
    NSNumber *aNumber = @5;
    NSNumber *anotherNumber = @6;
    MyLinkedList *aList = [MyLinkedList new];

    [aList appendObject:aNumber];
    [aList appendObject:anotherNumber];

    XCTAssertEqual([aList count], 2, @"List should have 2 elements but has %lu elements!", [aList count]);
    XCTAssertEqual([aList objectAtPosition:0], aNumber, @"First object should be %@ but is %@!", aNumber, [aList objectAtPosition:0]);
    XCTAssertEqual([aList objectAtPosition:1], anotherNumber, @"Second object should be %@ but is %@!", aNumber, [aList objectAtPosition:1]);
}

- (void)testInsertObjectFront {
    NSNumber *aNumber = @5;
    MyLinkedList *aList = [[MyLinkedList alloc] initWithObjects:@[@1, @2]];

    [aList insertObject:aNumber atPosition:0];

    XCTAssertEqual([aList count], 3, @"List should have 3 elements but has %lu elements!", [aList count]);
    XCTAssertEqual([aList objectAtPosition:0], aNumber, @"First object should be %@ but is %@!", aNumber, [aList objectAtPosition:0]);
    XCTAssertEqual([aList objectAtPosition:1], @1, @"Second object should be %@ but is %@!", @1, [aList objectAtPosition:1]);
    XCTAssertEqual([aList objectAtPosition:2], @2, @"Third object should be %@ but is %@!", @2, [aList objectAtPosition:2]);
}

- (void)testInsertObjectMiddle {
    NSNumber *aNumber = @5;
    MyLinkedList *aList = [[MyLinkedList alloc] initWithObjects:@[@0, @2]];

    [aList insertObject:aNumber atPosition:1];

    XCTAssertEqual([aList count], 3, @"List should have 3 elements but has %lu elements!", [aList count]);
    XCTAssertEqual([aList objectAtPosition:0], @0, @"First object should be %@ but is %@!", @0, [aList objectAtPosition:0]);
    XCTAssertEqual([aList objectAtPosition:1], aNumber, @"Second object should be %@ but is %@!", aNumber, [aList objectAtPosition:1]);
    XCTAssertEqual([aList objectAtPosition:2], @2, @"Third object should be %@ but is %@!", @2, [aList objectAtPosition:2]);
}

- (void)testInsertObjectBack {
    NSNumber *aNumber = @5;
    MyLinkedList *aList = [[MyLinkedList alloc] initWithObjects:@[@0, @1]];

    [aList insertObject:aNumber atPosition:2];

    XCTAssertEqual([aList count], 3, @"List should have 3 elements but has %lu elements!", [aList count]);
    XCTAssertEqual([aList objectAtPosition:0], @0, @"First object should be %@ but is %@!", @0, [aList objectAtPosition:0]);
    XCTAssertEqual([aList objectAtPosition:1], @1, @"Second object should be %@ but is %@!", @1, [aList objectAtPosition:1]);
    XCTAssertEqual([aList objectAtPosition:2], aNumber, @"Third object should be %@ but is %@!", aNumber, [aList objectAtPosition:2]);
}

- (void)compareList:(MyLinkedList*)list toReversedList:(MyLinkedList*)reversedList {
    for(int i = 0, e = (int)[list count] - 1; i <= e; ++i, --e)
    {
        XCTAssertEqual([list objectAtPosition:i], [reversedList objectAtPosition:e], @"Object at position %d of the reversed list should be %@ but was %@!", e, [list objectAtPosition:i], [reversedList objectAtPosition:e]);
    }
}

- (void)testReverse {
    MyLinkedList *aList = [[MyLinkedList alloc] initWithObjects:@[@0, @1, @2, @3]];

    MyLinkedList *reversedList = [MyLinkedList reverseLinkedList:aList];

    XCTAssertNotNil(reversedList, @"Reversed list should not be nil!");
    XCTAssertEqual(&reversedList, &aList, @"The reversed list should be the same object as the input list!");

    [self compareList:aList toReversedList:reversedList];
}

- (void)testReverseStatic {
    MyLinkedList *aList = [[MyLinkedList alloc] initWithObjects:@[@0, @1, @2, @3]];

    MyLinkedList *reversedList = [MyLinkedList reverseLinkedList:aList];

    XCTAssertNotNil(reversedList, @"Reversed list should not be nil!");
//    XCTAssertEqual(aList, listReference, @"The input list was changed!");
    XCTAssertNotEqual(&reversedList, &aList, @"The reversed list should be a different object than the input list!");

    [self compareList:aList toReversedList:reversedList];
}

- (void)testPerformanceBubbleSort {
    NSArray *objects = @[@5, @(-1), @0, @10];
    MyLinkedList *aList = [[MyLinkedList alloc] initWithObjects:objects];

    if([MyLinkedList respondsToSelector:@selector(sortUsingBubbleSort)])
    {
        [self measureBlock:^{
            [aList sortUsingBubbleSort];
        }];
    }
}

@end
