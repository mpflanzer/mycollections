//
//  MyElementTests.m
//  MyCollections
//
//  Created by Moritz Pflanzer on 12/03/2016.
//  Copyright © 2016 Moritz Pflanzer. All rights reserved.
//

#import "MyElement.h"
#import <XCTest/XCTest.h>

@interface MyElementTests : XCTestCase

@end

@implementation MyElementTests

- (void)testDefaultCreation {
    MyElement *emptyElement = [MyElement new];

    XCTAssertNotNil(emptyElement, @"Creation of empty element failed");
}

- (void)testCreationWithInit {
    NSNumber *aNumber = @5;
    MyElement *anElement = [[MyElement alloc] initWithObject:aNumber];

    XCTAssertNotNil(anElement, @"Creation of empty element failed");
    XCTAssertEqual(anElement.object, aNumber, @"Element not correct initialised. Object should be %@ but is %@", aNumber, anElement.object);
}

- (void)testSetValue {
    NSNumber *aNumber = @5;
    MyElement *anElement = [MyElement new];

    anElement.object = aNumber;

    XCTAssertEqual(anElement.object, aNumber, @"Value not set correctly. Object should be %@ but is %@", aNumber, anElement.object);
}

@end
